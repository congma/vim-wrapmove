" wrapmove: plugin for moving around using hjkl and cohort while wrapping
" text.
" Reference:
" http://vim.wikia.com/wiki/Move_cursor_by_display_lines_when_wrapping

if exists("g:loaded_wrapmove_autoload")
  finish
endif
let g:loaded_wrapmove_autoload = 1

let s:save_cpo = &cpo
set cpo&vim

let s:wm_gmove = 0

" mapping to make movements operate on 1 screen line in wrap mode
function! wrapmove#ScreenMovement(movement)
  if &wrap && s:wm_gmove
    return "g" . a:movement
  else
    return a:movement
  endif
endfunction

function! wrapmove#SetBreakMove()
  let s:wm_gmove = 1
endfunction

function! wrapmove#UnSetBreakMove()
  let s:wm_gmove = 0
endfunction

function! wrapmove#TYToggleBreakMove()
  let s:wm_gmove = !s:wm_gmove
  echo "Emulating movements in wrapped text: " . (s:wm_gmove? "yes" : "no")
endfunction

let &cpo = s:save_cpo
