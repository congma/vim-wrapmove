" wrapmove: plugin for moving around using hjkl and cohort while wrapping
" text.
" This is the mapping file.
" Reference:
" http://vim.wikia.com/wiki/Move_cursor_by_display_lines_when_wrapping

if exists("g:loaded_wrapmove")
  finish
endif
let g:loaded_wrapmove = 1

let s:save_cpo = &cpo
set cpo&vim

onoremap <silent> <expr> j wrapmove#ScreenMovement("j")
onoremap <silent> <expr> k wrapmove#ScreenMovement("k")
onoremap <silent> <expr> 0 wrapmove#ScreenMovement("0")
onoremap <silent> <expr> ^ wrapmove#ScreenMovement("^")
onoremap <silent> <expr> $ wrapmove#ScreenMovement("$")
nnoremap <silent> <expr> j wrapmove#ScreenMovement("j")
nnoremap <silent> <expr> k wrapmove#ScreenMovement("k")
nnoremap <silent> <expr> 0 wrapmove#ScreenMovement("0")
nnoremap <silent> <expr> ^ wrapmove#ScreenMovement("^")
nnoremap <silent> <expr> $ wrapmove#ScreenMovement("$")
vnoremap <silent> <expr> j wrapmove#ScreenMovement("j")
vnoremap <silent> <expr> k wrapmove#ScreenMovement("k")
vnoremap <silent> <expr> 0 wrapmove#ScreenMovement("0")
vnoremap <silent> <expr> ^ wrapmove#ScreenMovement("^")
vnoremap <silent> <expr> $ wrapmove#ScreenMovement("$")
vnoremap <silent> <expr> j wrapmove#ScreenMovement("j")


nmap  <expr> <Leader>b  wrapmove#TYToggleBreakMove()

let &cpo = s:save_cpo
